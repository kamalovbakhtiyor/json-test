class Task:
  def __init__(self, title=None, description=None) -> None:
    # Определить тип
    self.type = "task"

    # Определить содержание
    self.title = title
    self.description = description

  def to_object(self):
    return {
      "type": self.type, 
      "title": self.title, 
      "description": self.description
  	}
  
  def fields(self):
    return ["title", "description"]
