import json
import os


class Db:
  def __init__(self) -> None:
    # Если не существует файла базы данных
    if not os.path.exists("./db.json"):
      with open("./db.json", "w") as db:
        db.write("[]")

  def create(self, object):
    # Получить все объекты в Базе данных
    try:
      with open("./db.json") as file:
        self.objects = json.load(file)
    except:
      self.objects = []

    # Найти id последнего объекта из списка согласно типу
    self.out_filter = filter(lambda x: x["type"] == object["type"], self.objects)
    self.out_filter = list(self.out_filter)

    # Обозначить последний объект и кандидата
    self.last_object = self.out_filter[-1] if self.out_filter else None
    self.candidat = object

    if self.last_object:
      # Присвоить кандидату уникальный id
      self.candidat["id"] = self.last_object["id"] + 1
    else:
      # Присвоить кандидату id равное нулю
      self.candidat["id"] = 0

    # Добавить кандидата в список объектов
    self.objects.append(self.candidat)
 
    try:
      with open("./db.json", "w") as file:
        # Добавить кандидата в базу данных
        json.dump(self.objects, file, indent=2)

      return {"status": True, "data": self.candidat}
    except:
      return {"status": False, "message": "Не удалось создать объект"}
    
  def get(self, type=None, id=None):
    # Получить все объекты в Базе данных
    try:
      with open("./db.json") as file:
        self.objects = json.load(file)
    except:
      self.objects = []

    if len(self.objects) > 0:
      if type is not None and id is not None:
        objects = list(filter(lambda x: x["type"] == type, self.objects))
        object = list(filter(lambda x: x["id"] == id, objects))
        
        return object[0] if len(object) > 0 else {}
      
      if id is None:
        return list(filter(lambda x: x["type"] == type, self.objects))
      
      if type is None:
        print("Ошибка: не указан type")

        return {"status": False, "message": "Не указан type"}
    else:
      print("База данных пуста")

      return []
