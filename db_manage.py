import sys
import os
from settings import models


root_folder = os.path.dirname(os.path.abspath(__file__))
migrations_folder_path = f"{root_folder}/migrations"

def help():
  print("Help")

def makemigra():
  # Проверить файлы в папке migrations
  if os.path.exists(migrations_folder_path) and os.path.isdir(migrations_folder_path):
    # Проверить и внести все классы в папку migrations
    for model in models:
      if os.path.exists(f"{root_folder}/{model["name"]}.py"):
        # Проверить имеется ли файл модели в migrations
        if os.path.exists(f"{migrations_folder_path}/{model["name"]}.py"):
          # Проверить поля на изменность
          with open(f"{migrations_folder_path}/{model["name"]}.py") as file:
            model_fields_before = file.read()

          model_fields_after = model["fields"]

          if str(model_fields_before) == str(model_fields_after):
            print("yes", model_fields_before, model_fields_after)
          else:
            # Создаем файл модели в migrations
            make_file_models_to_migrations_folder(model=model)
        else:
          # Создаем файл модели в migrations
          make_file_models_to_migrations_folder(model=model)
  else:
    os.makedirs(migrations_folder_path)
    makemigra()

def make_file_models_to_migrations_folder(model):
  with open(f"{migrations_folder_path}/{model["name"]}.py", "w") as file:
    file.write(f"{model["fields"]}")

allowed_args = [
  {
    "name": "--help",
    "command": help,
  },
  {
    "name": "makemigra",
    "command": makemigra,
  },
]

if len(sys.argv) > 0:
  for arg in sys.argv[1:]:
    command = list(filter(lambda x: x["name"] == arg, allowed_args))
    command[0]["command"]() if len(command) > 0 else print("Не удалось распознать аргумент")
else:
  print("Требуется ввести аргументы. Для справки введите --help")
